﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Website..Master" AutoEventWireup="true" CodeBehind="Landing.aspx.cs" Inherits="assignement5.WebForm4" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">

    <h1>&nbsp;</h1>
    <h1>&nbsp;</h1>
    <h2>WELCOME TO INTERNET PROGRAMMING</h2>
<p>An Internet application is a client-server application that uses standard Internet protocols for connecting the client to the server. You can use exactly the same techniques to create a true Internet application, which is available publicly through the World-Wide Web, or to create an intranet application. An intranet application is one which runs on your corporate intranet, and is only available to the staff in your corporation. Whenever we talk about Internet applications, we mean either true Internet applications or intranet applications. </p>
<h1><a name="s003">What does an Internet Application Look Like?</a></h1>
<p>When the end-user clicks the <b>Send Form</b> button, the information on the form is packaged, and sent to a server-side program. The server-side program only runs when it is started from a form, or from a link on a Web page. The server-side program processes the information on the form, and returns a result to the end-user. </p>
<h1><a name="s005">Execution Flow</a></h1>
<p>
    A server-side program only runs for long enough to process some data and return a result. The end-user has the illusion that the application is running all the time, when the application in fact consists of a series of transactions which run for a short time and then disappear.</p>
<p>
    An application based on a symmetric server-side program (see the previous section for an explanation of this term) works like this:
</p>
<ol>
    <li>An end-user clicks a link on a Web page which starts a server-side program running.<br />
        <br />
    </li>
    <li>The server-side program runs, and sends back an HTML page, containing a form for the application.<br />
        <br />
    </li>
    <li>The end-user fills in the form and clicks a button to submit it.
        <p>
            This runs the server-side program again.</p>
    </li>
    <li>The server-side program fetches some data, and returns it to the end-user&#39;s browser in the form.</li>
</ol>
<p>
    As devices communicate not just with human consumers, but with other devices, fundamentally new capabilities emerge. It&#39;s not only that your refrigerator will know you have run out of tomatoes, but that it can place an order for more on your behalf. The success of <a href="http://www.computer.org/portal/web/computingnow/archive/september2013" target="_blank">pervasive computing</a> will be that it recedes into the background, working out facts and events and remedies with other connected devices. Only executive-level results will be communicated to human consumers. The triumph of IoT will be in all the things that we <a href="https://hbr.org/2014/11/the-house-that-actually-makes-the-internet-of-things-easy" target="_blank">no longer have to think about</a>, even as they are seamlessly done for us.</p>
<p>&nbsp;</p>


</asp:Content>
