# Gym-Site-IP
Internet Programming project for us all to branch. works easier.


This works well to split up each section. 
fork your own copy. when you make major changes, pull request it and add notes.

# Practices
## for divided workloads. 
use this master as a starting point

::> create a fork from master repository to make your own repo specific for your tasks.

::> once work is ready to add to master branch. <pull request> to origin/master.

::> this leaves the owners of master review added files and MERGE master repo with the forked repo.


## Usage Example
cameron's tasks are the store. 
->cameron forks master repository and names it (Gym-Site-IP-Store).

->Cameron <commits> new web pages to his forked repo.

->After which cameron can <pull request> his repo with the main repository.
